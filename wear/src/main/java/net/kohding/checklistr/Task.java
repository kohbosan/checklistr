package net.kohding.checklistr;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.GridPagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.TreeMap;

public class Task extends WearableActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        LinearLayout ll = (LinearLayout)findViewById(R.id.bottombar);
        ll.setOnClickListener(this);
    }

    public void onClick(View v){
        Intent intent = new Intent(getApplicationContext(), Task.class);
        startActivity(intent);
    }

    public class gridPagerAdapter extends GridPagerAdapter {

        private class Plane {

            String planeName;
            Drawable icon;
            TreeMap<String, String[][]> checklists;
            String[][] tasks;

            Plane(String name, Drawable icon){
                this.planeName = name;
                this.icon = icon;
                checklists = new TreeMap<>();
                tasks = new String[][]{
                        {"Control Wheel Lock", "Remove"},
                        {"Ignition Switch" ,"Off"},
                        {"Master Switch" ,"On"},
                        {"Fuel Quantity Indicators" ,"Check Quantity "},
                        {"Master Switch" ,"Off"},
                        {"Baggage Door" ,"Check/ Lock"},
                };
                checklists.put("Preflight Inspection", tasks);
            }
        }

        final Plane planes[] = new Plane[] {
                new Plane("1975 Cessna 172m", getResources().getDrawable(R.drawable.cessna172, null)),
                new Plane("1976 Cessna 172m", getResources().getDrawable(R.drawable.cessna172, null))
        };

        // Obtain the number of pages (vertical)
        @Override
        public int getRowCount() {
            return planes.length;
        }

        // Obtain the number of pages (horizontal)
        @Override
        public int getColumnCount(int rowNum) {
            return planes[rowNum].checklists.size()+1;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int row, int col) {
            TextView t = new TextView(getApplicationContext());
            t.setText("Sup....");
            container.addView(t);
            return t;
        }

        @Override
        public void destroyItem(ViewGroup container, int row, int col, Object o) {
            container.removeView((View)o);
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view == o;
        }

        /*// Obtain the UI fragment at the specified position
        @Override
        public Fragment getFragment(int row, int col) {
            Plane plane = planes[row];
            String title = null;
            if(col == 0)
            {
                title = plane.planeName;
            }
            else
            {
                NavigableSet<String> keys = plane.checklists.navigableKeySet();
                Iterator<String> itr = keys.iterator();
                int index = 0;
                while(itr.hasNext()){
                    if(index++ != col){
                        itr.next();
                        continue;
                    }
                    title = itr.next();
                }
            }
            ItemFragment fragment = (ItemFragment)ItemFragment.create(title, null);

            return fragment;
        }*/
    }
}


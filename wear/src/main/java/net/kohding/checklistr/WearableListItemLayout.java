package net.kohding.checklistr;

import android.content.Context;
import android.support.wearable.view.WearableListView;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import net.kohding.checklistr.R;

public class WearableListItemLayout extends LinearLayout implements WearableListView.OnCenterProximityListener {

    private TextView mName;
    private ImageView icon;

    private final float mFadedTextAlpha;

    public WearableListItemLayout(Context context) {
        this(context, null);
    }

    public WearableListItemLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WearableListItemLayout(Context context, AttributeSet attrs,
                                  int defStyle) {
        super(context, attrs, defStyle);

        mFadedTextAlpha = getResources()
                .getInteger(R.integer.action_text_faded_alpha) / 100f;
    }

    // Get references to the icon and text in the item layout definition
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        // These are defined in the layout file for list items
        // (see next section)
        icon = (ImageView) findViewById(R.id.icon);
        icon.setImageDrawable(getResources().getDrawable(R.drawable.cessna172, null));
        mName = (TextView) findViewById(R.id.name);
    }

    @Override
    public void onCenterPosition(boolean animate) {
        mName.setAlpha(1f);
        icon.setImageAlpha(255);
    }

    @Override
    public void onNonCenterPosition(boolean animate) {
        icon.setImageAlpha(128);
        mName.setAlpha(mFadedTextAlpha);
    }
}
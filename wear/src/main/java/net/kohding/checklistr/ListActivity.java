package net.kohding.checklistr;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.view.WearableListView;

public class ListActivity extends Activity implements WearableListView.ClickListener {

        // Sample dataset for the list
        String[] elements = { "1975 Cessna 172M", "Piper Cadet" };

        @Override
        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_airplane_chooser);

                // Get the list component from the layout of the activity
                WearableListView listView =
                (WearableListView) findViewById(R.id.wearable_list);

                // Assign an adapter to the list
                listView.setAdapter(new Adapter(this, elements));

                // Set a click listener
                listView.setClickListener(this);
        }

        // WearableListView click listener
        @Override
        public void onClick(WearableListView.ViewHolder v) {
                Integer tag = (Integer) v.itemView.getTag();
                // use this data to complete some action ...
                Intent intent = new Intent(this, Task.class);
                startActivity(intent);
        }

        @Override
        public void onTopEmptyRegionClick() {
        }
}
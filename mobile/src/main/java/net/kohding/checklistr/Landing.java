package net.kohding.checklistr;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;

public class Landing extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        ImageView logo = (ImageView)findViewById(R.id.CenterLogo);
        logo.setImageResource(R.mipmap.ic_launcher);
    }
}
